/*
 * Acetate.hh
 *
 *  Created on: 24.10.2016
 *      Author: scholz
 */

/*!
 * \file
 * \ingroup Components
 * \brief A class for the Acetate fluid properties
 */
#ifndef DUMUX_ACETATE_HH
#define DUMUX_ACETATE_HH

#include <dumux/material/components/base.hh>
#include <dumux/material/components/liquid.hh>

namespace Dumux {
namespace Components {
/*!
 * \ingroup Components
 * \brief A class for the Acetate fluid properties
 */
template <class Scalar>
class Acetate
: public Components::Base<Scalar, Acetate<Scalar> >
, public Components::Liquid<Scalar, Acetate<Scalar> >
{
public:
   /*!
    * \brief A human readable name for the Acetate.
    */
    static const char *name()
    { return "Acetate"; }

   /*!
    * \brief The molar mass in \f$\mathrm{[kg/mol]}\f$ of one mole of Acetate.
    */
    static Scalar molarMass()
    { return 60e-3; } // kg/mol CH3COOH

   /*!
    * \brief The diffusion Coefficient of Acetate in water.
    */
    static Scalar liquidDiffCoeff(Scalar temperature, Scalar pressure)
    { return 1.5e-9; }
    //{ return 2e-9; } // Check the right value TODO
};

} // end namespace Components
} // end namespace Dumux

#endif
