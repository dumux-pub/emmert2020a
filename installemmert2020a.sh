#!/bin/sh

# dune-common
# releases/2.6 # f3cb9408388f3424b4f0a7a55cad84057b6b444d # 2018-12-13 17:16:34 +0000 # Jö Fahlke
git clone https://gitlab.dune-project.org/core/dune-common.git
cd dune-common
git checkout releases/2.6
git reset --hard f3cb9408388f3424b4f0a7a55cad84057b6b444d
cd ..

# dune-geometry
# releases/2.6 # 1eb09fa58d4894ead8df4dd235bd3d19f2b6ac38 # 2018-12-06 22:56:21 +0000 # Steffen Müthing
git clone https://gitlab.dune-project.org/core/dune-geometry.git
cd dune-geometry
git checkout releases/2.6
git reset --hard 1eb09fa58d4894ead8df4dd235bd3d19f2b6ac38
cd ..

# dune-grid
# releases/2.6 # 76f18471498824d49a6cecbfba520b221d9f79ca # 2018-12-10 14:35:11 +0000 # Steffen Müthing
git clone https://gitlab.dune-project.org/core/dune-grid.git
cd dune-grid
git checkout releases/2.6
git reset --hard 76f18471498824d49a6cecbfba520b221d9f79ca
cd ..

# dune-localfunctions
# releases/2.6 # ee794bfdfa3d4f674664b4155b6b2df36649435f # 2018-12-06 23:40:32 +0000 # Steffen Müthing
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
cd dune-localfunctions
git checkout releases/2.6
git reset --hard ee794bfdfa3d4f674664b4155b6b2df36649435f
cd ..

# dune-istl
# releases/2.6 # 9698e497743654b6a03c219b2bdfc27b62a7e0b3 # 2018-12-14 10:00:30 +0000 # Jö Fahlke
git clone https://gitlab.dune-project.org/core/dune-istl.git
cd dune-istl
git checkout releases/2.6
git reset --hard 9698e497743654b6a03c219b2bdfc27b62a7e0b3
cd ..

# dune-foamgrid
# releases/2.6 # 20f7851cac039627402419db6cb5850b4485c871
git clone https://gitlab.dune-project.org/extensions/dune-foamgrid.git
cd dune-foamgrid
git checkout releases/2.6
git reset --hard 20f7851cac039627402419db6cb5850b4485c871
cd ..


# dumux
# master # 783981dbd62d758a80f3bee9435a37a08f89d2f9
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
cd dumux
git checkout master
git reset --hard 783981dbd62d758a80f3bee9435a37a08f89d2f9
cd ..

# the pub-module containing the code for the examples
git clone https://git.iws.uni-stuttgart.de/dumux-pub/emmert2020a

# configure project
./dune-common/bin/dunecontrol --opts=dumux/cmake.opts all
