# Emmert2020a

Welcome to the dumux pub module for Emmert2020a
======================================================

This module contains the source code for the examples in the
paper

__S. Emmert et al. (2020)__, _[Importance of specific substrate utilization by microbes in microbially enhanced coal-bed methane production: A modelling study](https://doi.org/10.1016/j.coal.2020.103567)_ (International Journal of Coal Geology) 

``` 
@article{emmert2020importance,
    title={Importance of specific substrate utilization by microbes in microbially enhanced coal-bed methane production: A modelling study},
    author={Emmert, Simon and Class, Holger and Davis, Katherine J and Gerlach, Robin},
    journal={International Journal of Coal Geology},
    volume={229},
    pages={103567},
    year={2020},
    issn = {0166-5162},
    doi = {https://doi.org/10.1016/j.coal.2020.103567},
    url = {http://www.sciencedirect.com/science/article/pii/S0166516220305632},
    publisher={Elsevier}
}
```

For building from source create a folder and download `installemmert2020a.sh` from this repository.
```bash
chmod +x installemmert2020a.sh
./installemmert2020a.sh
```
will download configure and compile all dune dependencies. Furthermore you need to have the following basic requirement installed

* C, C++ compiler (C++14 required)
* Fortran compiler (gfortran)
* UMFPack from SuiteSparse



## Dependencies on other DUNE modules

| module | branch | commit hash |
|:-------|:-------|:------------|
| dune-common | releases/2.6 | f3cb9408388f3424b4f0a7a55cad84057b6b444d |
| dune-geometry | releases/2.6 | 1eb09fa58d4894ead8df4dd235bd3d19f2b6ac38 |
| dune-grid | releases/2.6 | 76f18471498824d49a6cecbfba520b221d9f79ca |
| dune-localfunctions | releases/2.6 | ee794bfdfa3d4f674664b4155b6b2df36649435f |
| dune-istl | releases/2.6 | 9698e497743654b6a03c219b2bdfc27b62a7e0b3 |
| dune-foamgrid | releases/2.6 | 20f7851cac039627402419db6cb5850b4485c871 |
| dumux | master | 783981dbd62d758a80f3bee9435a37a08f89d2f9 |

The examples are in the folder
```
emmert2020a/build-cmake/appl/mecbm/batch

```

All examples are compiled by the following commands

```bash
cd emmert2020a/build-cmake/
make test_mecbmbatch_tpfa
```

Run an example (e.g. Coal+++ case is identified with Coal111)
```bash
cd emmert2020a/build-cmake/appl/mecbm/batch
./test_mecbmbatch_tpfa test_mecbmbatchCoal111.input
```



